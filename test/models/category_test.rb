require 'test_helper'

class CategoryTest < ActiveSupport::TestCase
  test 'simple' do
    category = categories(:one)
    category.url = '1'*rand(5..100)
    category.name = '1'*rand(5..250)

    # TODO check needing test validate parent presence false
    category.parent_id = rand(2).zero? ? nil : 1

    assert category.valid?
  end

  test 'validate parent presence false' do
    category = categories(:one)

    category.parent_id = nil
    assert category.valid?

    category.parent_id = 1
    assert category.valid?
  end

  test 'url length < 5' do
    category = categories(:one)
    category.url = '1'*4

    assert_not category.valid?
  end

  test 'url length > 100' do
    category = categories(:one)
    category.url = '1'*101

    assert_not category.valid?
  end

  test 'name length < 5' do
    category = categories(:one)
    category.name = '1'*4

    assert_not category.valid?
  end

  test 'name length > 250' do
    category = categories(:one)
    category.name = '1'*251

    assert_not category.valid?
  end

  test 'has many subcat' do
    category = categories(:one)

    # need correct fixtures (parents: 1 << 2, 2 << 3)
    assert category.subcategories.include?(categories(:two))
    assert category.subcategories.exclude?(categories(:three))
  end

  test 'scope catalog' do
    Category.catalogs.each do |catalog|
      assert catalog.parent.nil?
    end
  end
end
