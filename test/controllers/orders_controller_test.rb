require 'test_helper'

class OrdersControllerTest < ActionDispatch::IntegrationTest
  test 'should get order' do
    get order_url
    assert_redirected_to order_url
  end

  test 'should get delivery_info' do
    get delivery_info_url
    assert_response :success
  end

    # TODO fix empty client params in test
  # test 'should get make_order' do
  #   post make_order_url
  #   assert_redirected_to thankyou_url
  # end

  # TODO fix nil client in test
  # test 'should get thank_you' do
  #   get thankyou_url
  #   assert_response :success
  # end

end
