require 'test_helper'

class CartControllerTest < ActionDispatch::IntegrationTest
  test 'should get show' do
    get cart_root_url
    assert_response :success
  end

  test 'should get update' do
    get cart_update_url(books(:one))
    assert_response :success
  end

  test 'should get add' do
    post cart_add_url(books(:one))
    assert_response :success
  end

  test 'should get del' do
    post cart_del_url(books(:one))
    assert_redirected_to cart_root_url
  end

  test 'should get minus' do
    post cart_minus_url(books(:one))
    assert_response :success
  end

  test 'should get plus' do
    post cart_plus_url(books(:one))
    assert_response :success
  end

end
