require 'test_helper'

class CatalogsControllerTest < ActionDispatch::IntegrationTest

  test 'show page should get catalog title' do
    get catalog_path categories(:one).url

    assert_response :success
    assert_select 'h2', categories(:one).name
  end

  test 'should get search' do
    get search_path
    assert_response :success
  end

end
