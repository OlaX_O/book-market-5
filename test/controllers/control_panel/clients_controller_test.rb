require 'test_helper'

class ControlPanel::ClientsControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get cp_clients_url
    assert_response :success
  end

  test 'should get show' do
    get cp_client_url(clients(:one))
    assert_response :success
  end

  test 'should get edit' do
    get edit_cp_client_url(clients(:one))
    assert_response :success
  end

end
