require 'test_helper'

class ControlPanel::OrdersControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get cp_orders_url
    assert_response :success
  end

  test 'should get show' do
    get cp_order_url(orders(:one))
    assert_response :success
  end

  test 'should get new' do
    get new_cp_order_url
    assert_response :success
  end

  test 'should get edit' do
    get edit_cp_order_url(orders(:one))
    assert_response :success
  end

end
