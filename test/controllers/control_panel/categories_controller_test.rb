require 'test_helper'

class ControlPanel::CategoriesControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get cp_categories_url
    assert_response :success
  end

  test 'should get show' do
    get cp_category_url(categories(:one))
    assert_response :success
  end

  test 'should get new' do
    get new_cp_category_url
    assert_response :success
  end

  test 'should get edit' do
    get edit_cp_category_url(categories(:one))
    assert_response :success
  end

end
