require 'test_helper'

class ControlPanel::BooksControllerTest < ActionDispatch::IntegrationTest
  test 'should get index' do
    get cp_books_url
    assert_response :success
  end

  test 'should get show' do
    get cp_book_url(books(:one))
    assert_response :success
  end

  test 'should get new' do
    get new_cp_book_url
    assert_response :success
  end

  test 'should get edit' do
    get edit_cp_book_url(books(:one))
    assert_response :success
  end

end
