require_relative 'boot'

require 'rails/all'

require 'carrierwave'
require 'carrierwave/orm/activerecord'

Bundler.require(*Rails.groups)

module Bm5
  class Application < Rails::Application

    # config.i18n.default_locale = 'ru'

  end
end
