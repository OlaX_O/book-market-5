Rails.application.routes.draw do

  root 'pages#index'

  scope :catalog, only: [:index, :show] do
    # old book-market links format to google & yandex
    get '/:category', to: 'catalogs#show', as: 'catalog'
    get '/:category/:url', to: 'books#show', as: 'book'
  end

  get '/search', to: 'catalogs#search'

  %w(tile big small full).each do |type|
    get "/book_image_#{type}/:image_id", to: "books#image_#{type}", as: "book_image_#{type}"
  end

  scope 'cart', as: :cart do
    root 'cart#show', as: 'root'

    get '/update', to: 'cart#update'

    post ':book_id/minus', to: 'cart#minus', as: :minus
    post ':book_id/plus', to: 'cart#plus', as: :plus
    post ':book_id/add', to: 'cart#add', as: :add
    post ':book_id/del', to: 'cart#del', as: :del
  end

  # TODO order scope
  get '/order', to: 'orders#order'
  get '/delivery_info', to: 'orders#delivery_info'
  post '/delivery_info', to: 'orders#make_order', as: 'make_order'
  get '/thankyou', to: 'orders#thank_you'

  # scope 'order', as: :order do
  #   root 'order#show'
  #
  #   get '/delivery', to: 'orders#delivery'
  #   post '/create', to: 'orders#create'
  #   get '/thankyou', to: 'orders#thank_you'
  # end

  # TODO add pages routes

  namespace :control_panel, as: 'cp', path: 'cp' do
    root 'dashboard#index', as: 'root'

    resources :categories, :orders, :clients

    resources :books do
      get :images
      patch :image_upload
      get :image_delete
    end
  end

end
