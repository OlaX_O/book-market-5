FactoryGirl.define do
  factory :book do
    name { Faker::Book.title }
    contents { Faker::Lorem.sentences }
    url { Faker::Internet.password(8) }
    text { Faker::Lorem.paragraphs }
    author { Faker::Book.author }
    cover 'Doe'
    price { Faker::Number.decimal(3) }

    available { Faker::Boolean.boolean }

    # created_at
    # updated_at

    # category category_id

    # images
  end
end
