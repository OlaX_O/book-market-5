class CreateBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :books do |t|
      t.string :name, null: false, index: true
      t.string :contents
      t.string :url, null: false, index: true
      t.text :text
      t.string :author, index: true
      t.string :cover
      t.integer :pages
      t.references :category, index: true
      t.decimal :price, precision: 5, scale: 2, default: 0
      t.boolean :available, index: true

      t.timestamps
    end
  end
end
