class CreateOrderBooks < ActiveRecord::Migration[5.0]
  def change
    create_table :order_books do |t|
      t.references :book, foreign_key: true
      t.references :order, index: true, foreign_key: true
      t.decimal :price, precision: 5, scale: 2
      t.integer :quantity

      t.timestamps
    end
  end
end
