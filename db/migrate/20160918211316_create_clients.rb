class CreateClients < ActiveRecord::Migration[5.0]
  def change
    create_table :clients do |t|
      t.string :name, null: false
      t.string :address
      t.string :phone, null: false
      t.string :email
      t.text :notes
      t.references :delivery
      t.references :payment

      t.timestamps
    end

    add_reference :orders, :client, index: true
  end
end
