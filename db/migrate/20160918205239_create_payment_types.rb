class CreatePaymentTypes < ActiveRecord::Migration[5.0]
  def change
    create_table :payment_types do |t|
      t.string :name, null: false
      t.string :symbol, null: false
      t.boolean :available, null: false, default: true

      t.timestamps
    end
  end
end
