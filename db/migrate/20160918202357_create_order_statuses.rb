class CreateOrderStatuses < ActiveRecord::Migration[5.0]
  def change
    create_table :order_statuses do |t|
      t.string :name, null: false
      t.string :symbol, null: false

      t.timestamps
    end
  end
end
