class CreateOrders < ActiveRecord::Migration[5.0]
  def change
    create_table :orders do |t|
      t.decimal :total, precision: 6, scale: 2
      t.references :status
      t.string :session

      t.timestamps
    end
  end
end
