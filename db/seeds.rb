# Categories
puts 'Categories'
[
    {position: 1, name: 'Книги', url: 'books'},
    {position: 2, name: 'Обучающая литература', url: 'obuchayuschaya_literatura'},
    {position: 3, name: 'Пазлы', url: 'pazly'},
    {position: 4, name: 'Раскраски', url: 'raskraski'},
    {position: 5, name: 'Прочее', url: 'prochee'},
    {position: 6, name: 'Детское творчество', url: 'detskoe_tvorchestvo'},

    {parent: 'books', position: 1, name: 'Энциклопедии', url: 'entsiklopedii'},
    {parent: 'books', position: 2, name: 'Книги с наклейками', url: 'knigi_s_nakleykami'},
    {parent: 'books', position: 3, name: 'Развивающая литература', url: 'razvivayuschaya_literatura'},
    {parent: 'books', position: 4, name: 'В мягком переплёте маленького формата', url: 'v_myagkom_pereplete_malenkogo_formata'},
    {parent: 'books', position: 5, name: 'В мягком переплёте среднего формата', url: 'knigi_v_myagkom_pereplete'},
    {parent: 'books', position: 6, name: 'В мягком переплёте большого формата', url: 'knigi_v_myagkom_pereplete_bolshogo_forma'},
    {parent: 'books', position: 7, name: 'В твёрдом переплёте среднего формата', url: 'hudozhestvennaya_literatura'}, # hudozhestvennaya_literatura ?
    {parent: 'books', position: 8, name: 'В твёрдом переплёте большого формата', url: 'skazki'}, # skazki ?
    {parent: 'books', position: 9, name: 'Книги-картонки маленького формата', url: 'knigi_kartonki_malenkogo_formata'},
    {parent: 'books', position: 10, name: 'Книги-картонки среднего формата', url: 'knigi_kartonki'},
    {parent: 'books', position: 11, name: 'Книги-картонки большого формата', url: 'knigi-kartonki_bolshogo_formata'},

    {parent: 'obuchayuschaya_literatura', position: 1, name: 'Читаем по слогам', url: 'chitaem_po_slogam'},
    {parent: 'obuchayuschaya_literatura', position: 2, name: 'Карточки обучающие', url: 'kartochki_obuchayuschie'},
    {parent: 'obuchayuschaya_literatura', position: 3, name: 'Читаем по слогам', url: 'gotovye_domashnie_zadaniya'},
    {parent: 'obuchayuschaya_literatura', position: 4, name: 'Прописи и рабочие тетради', url: 'propisi'},
    {parent: 'obuchayuschaya_literatura', position: 5, name: 'Изучаем время и часы', url: 'izuchaem_vremya_i_chasy'},
    {parent: 'obuchayuschaya_literatura', position: 6, name: 'Буквари и азбуки', url: 'bukvari_i_azbuki'},
    {parent: 'obuchayuschaya_literatura', position: 7, name: 'Английский язык', url: 'angliyskiy_yazyk'},

    # TODO add others categories
].each do |r|
  Category.where(url: r[:url], name: r[:name])
      .first_or_create!(
          position: r[:position],
          name: r[:name],
          url: r[:url],
          parent: Category.find_by_url(r[:parent])
      )
end
