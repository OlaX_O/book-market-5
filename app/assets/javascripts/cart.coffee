$(document).on 'turbolinks:load', ->
  $(".book_buy_button, .book_tile_buy-button").on "click", ->
    $.ajax
      url: '/cart/' + $(this).attr('data-id') + '/add',
      type: 'POST',
      success: ->
        $("#cart").load('/cart/update')

  $(".cart_tile_del").on "click", ->
    book = $(this).parents('li')[0]
    $.ajax
      url: '/cart/' + $(this).attr('data-id') + '/del',
      type: 'POST',
      success: (data)->
        $(book).fadeOut(200)
        $("#cart").load('/cart/update')
        $("#cart_total").html(data['total'])

  $('.minus').on "click", ->
    input = $(this).parent().children("input")
    $.ajax
      url: '/cart/' + $(input).attr('data-id') + '/minus',
      type: 'POST',
      success: (data) ->
        $("#cart").load('/cart/update')
        input.val(data['new_count'])
        $("#cart_total").html(data['total'])

  $('.plus').on "click", ->
    input = $(this).parent().children("input")
    $.ajax
      url: '/cart/' + $(input).attr('data-id') + '/plus',
      type: 'POST',
      success: (data) ->
        $("#cart").load('/cart/update')
        input.val(data['new_count'])
        $("#cart_total").html(data['total'])
