class ControlPanel::ClientsController < ApplicationController

  layout 'control_panel'

  before_action :find_client, only: [:show, :edit]

  def index
    @clients = Client.order(id: :desc)
    @clients_count = {all: Client.count}
  end

  def new
  end

  def show
  end

  def edit
  end

  private

  def find_client
    @client = Client.find params[:id]
  end

end
