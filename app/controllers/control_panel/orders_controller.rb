class ControlPanel::OrdersController < ApplicationController

  layout 'control_panel'

  before_action :find_order, only: [:show, :edit, :update]

  def index
    @orders = Order.includes(:status).order(id: :desc)
    @orders_count = {all: Order.count}
  end

  def new
    @order = Order.new
    @order.books.build
  end

  def create
    @order = Order.new order_params(:url)
    render action: @order.save ? :show : :new
  end

  def show
  end

  def edit
    @order.books.build
  end

  def update
    @order.update order_params
    render action: @order.save ? :show : :edit
  end

  private

  def find_order
    @order = Order.find params[:id]
  end

  def order_params(*args)
    params.require(:order).permit(fields << args)
  end

  def fields
    [:status_id, books_attributes: [:_destroy, :id, :book_id, :quantity, :price]]
  end

end
