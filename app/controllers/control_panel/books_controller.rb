class ControlPanel::BooksController < ApplicationController

  layout 'control_panel'

  def index
    @books = Book.order(id: :desc).first(10)
  end

  def new
    @book = Book.new
  end

  def create
    @book = Book.new book_params(:url)

    if @book.save
      redirect_to(controller: '/control_panel/books', action: 'edit', id: @book)
    else
      render 'new'
    end
  end

  def show
    @book = Book.find params[:id]
  end

  def edit
    @book = Book.find params[:id]
  end

  def update
    @book = Book.find params[:id]
    @book.update_attributes(book_params)

    redirect_to(controller: '/control_panel/books', action: 'show', id: @book)
  end

  def images
    @book = Book.find params[:book_id]
    render 'images', layout: false
  end

  def image_upload
    @book = Book.find params[:book_id]
    image = @book.images.build
    image.image = params[:file]

    if @book.save
      render json: :success
    else
      render json: {error: @book.errors[:file]}, status: 422
    end
  end

  def image_delete
    @book = Book.find params[:book_id]
    if params[:img].present?
      @book.images.find(params[:img]).destroy
    end

    render 'edit'
  end

  private

  def book_params(*args)
    params.require(:book).permit(fields << args)
  end

  def fields
    [:text, :name, :price, :category_id, :available, image_attributes: :image]
  end

end
