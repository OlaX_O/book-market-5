class ControlPanel::CategoriesController < ApplicationController

  layout 'control_panel'

  def index
    @categories = Category.includes(:parent).all
  end

  def new
    @category = Category.new
  end

  def create
    @category = Category.new category_params(:url)

    if @category.save
      redirect_to cp_categories_path
    else
      render 'new'
    end
  end

  def show
    @category = Category.find params[:id]
  end

  def edit
    @category = Category.find params[:id]
  end

  def update
    @category = Category.find params[:id]
    @category.update! category_params

    redirect_to cp_categories_path
  end

  private

  def category_params(*args)
    params.require(:category).permit(fields << args)
  end

  def fields
    [:name, :parent_id]
  end

end
