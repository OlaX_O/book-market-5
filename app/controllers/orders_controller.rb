class OrdersController < ApplicationController

  # TODO rename create
  def order
    if session[:cart].is_a?(Hash) && session[:cart].any?
      @order = Order.new(session: session.id)
      @order.add_books order_books

      if @order.save
        redirect_to(controller: :orders, action: :delivery_info, total: @order.total.to_f)
      else
        redirect_to(controller: :orders, action: :order)
      end

    else
      redirect_to(controller: :orders, action: :order)
    end
    # TODO order params (name, address, payment etc)
  end

  # TODO rename to ordering client delivery info
  def delivery_info
    # TODO create client or find

    # TODO assigning client to order ans save

    @client = Client.new(
        name: params[:name],
        phone: params[:phone],
        address: params[:address],
        delivery_id: params[:delivery_id],
        payment_id: params[:payment_id],
        notes: params[:notes],
        email: params[:email]
    )
    @order_total = params[:total]
  end

  # TODO rename to order
  def make_order
    @order = Order.find_by_session session.id

    @client = @order.build_client(
        name: params[:client][:name],
        phone: params[:client][:phone],
        address: params[:client][:address],
        delivery_id: params[:client][:delivery_id],
        payment_id: params[:client][:payment_id],
        notes: params[:client][:notes],
        email: params[:client][:email]
    )

    if @order.save
      redirect_to(controller: :orders, action: :thank_you)
    else
      redirect_to(controller: :orders, action: :delivery_info, params: params[:client])
    end
  end

  def thank_you
    @order = Order.find_by_session session.id
    session[:cart] = []
    # TODO order complete
  end

  private

  def order_books
    Book.where(id: session[:cart].map { |id, _| id }).map { |book| {book: book, qty: session[:cart][book.id.to_s]} }
  end

end
