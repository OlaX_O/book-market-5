class BooksController < ApplicationController

  def show
    @book = Book.find_by(url: params[:url])
  end

  def image_full
    send_image BookImage.find(params[:image_id])
  end

  def image_big
    send_image BookImage.find(params[:image_id]), :big
  end

  def image_tile
    send_image BookImage.find(params[:image_id]), :tile
  end

  def image_small
    send_image BookImage.find(params[:image_id]), :small
  end

  private

  def send_image(image, type = nil)
    send_file (type ? image.image.versions[type].path : image.image.path), type: 'image/jpeg', disposition: 'inline'
  end

end
