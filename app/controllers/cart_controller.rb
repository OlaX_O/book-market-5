class CartController < ApplicationController

  def show
    @books = session[:cart].is_a?(Hash) ? Book.where(id: session[:cart].map{|id, _| id}) : []
  end

  def update
    render 'cart', layout: false
  end

  def add
    session[:cart] = {} unless session[:cart].is_a?(Hash)
    session[:cart][params[:book_id]] = 0 unless session[:cart][params[:book_id]].is_a?(Fixnum)
    session[:cart][params[:book_id]] += 1

    render json: {success: true}
  end

  def minus
    session[:cart] = {} unless session[:cart].is_a?(Hash)
    session[:cart][params[:book_id]] = 0 unless session[:cart][params[:book_id]].is_a?(Fixnum)
    session[:cart][params[:book_id]] -= 1
    session[:cart][params[:book_id]] = 1 if session[:cart][params[:book_id]] < 1

    render json: {success: true, new_count: session[:cart][params[:book_id]], total: calculate_total_price}
  end

  def plus
    session[:cart] = {} unless session[:cart].is_a?(Hash)
    session[:cart][params[:book_id]] = 0 unless session[:cart][params[:book_id]].is_a?(Fixnum)
    session[:cart][params[:book_id]] += 1
    session[:cart][params[:book_id]] = 99 if session[:cart][params[:book_id]] > 99

    render json: {success: true, new_count: session[:cart][params[:book_id]], total: calculate_total_price}
  end

  def del
    session[:cart] = {} unless session[:cart].is_a?(Hash)

    session[:cart].extract!(params[:book_id]) if session[:cart].has_key?(params[:book_id])

    if session[:cart].count > 0
      render json: {success: true, total: calculate_total_price}
    else
      redirect_to cart_root_path
    end
  end

  private

  def order_books
    Book.where(id: session[:cart].map { |id, _| id }).map { |book| {book: book, qty: session[:cart][book.id.to_s]} }
  end

  def calculate_total_price
    order_books.inject(0) {|sum, order_book| sum += order_book[:book].price.to_f * order_book[:qty]}
  end

end
