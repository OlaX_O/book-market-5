class CatalogsController < ApplicationController

  def index
    # @books = Book.includes(:images).last(10)
  end

  def show
    @catalog = Category.includes(:subcategories).find_by(url: params[:category])
    if @catalog
      # @books = Book.from_catalog(@catalog).paginate(page: params[:page])
    else
      redirect_to root_path
    end
  end

  def search
    # @books = Book.where('name like ?', "%#{params[:search]}%").paginate(page: params[:page])
  end

end
