class BookImageUploader < CarrierWave::Uploader::Base

  include CarrierWave::RMagick

  storage :file

  process resize_to_limit: [1000, 1000]

  version :tile do
    process resize_to_limit: [220, 220]
  end

  version :big do
    process resize_to_limit: [500, 500]
  end

  def extension_white_list
    %w(jpg jpeg png)
  end

  def store_dir
    "#{Rails.root}/uploads/#{model.class.to_s.underscore}/#{mounted_as}/#{model.id}"
  end

end
