class Order < ApplicationRecord

  belongs_to :status, class_name: 'OrderStatus'

  belongs_to :client
  accepts_nested_attributes_for :client

  has_many :books, class_name: 'OrderBook'
  accepts_nested_attributes_for :books, allow_destroy: true

  # validates_presence_of :status
  validates_presence_of :books
  # validates :total, numericality: {integer_only: true, greater_than: 0}, allow_blank: false

  before_validation :remove_empty

  def remove_empty
    self.books.each { |b| b.destroy! unless b.book_id }
  end

  def add_books(order_books)
    order_books.each do |order_book|
      self.books.build(
          book: order_book[:book],
          quantity: order_book[:qty],
          price: order_book[:book].price
      )
    end

    # REVIEW recalculate before save or after every book changes?
    recalculate_total
  end

  def recalculate_total
    self.total = books.inject(0) { |sum, order_book| sum += order_book.quantity * order_book.price }
  end

  private

  def set_status(status)
    self.status = OrderStatus.find_by_symbol(status)
  end

end
