class OrderBook < ApplicationRecord

  belongs_to :order
  belongs_to :book

  validates :quantity, numericality: {integer_only: true, greater_than: 0}, allow_blank: false
  validates :price, numericality: {greater_than: 0}, allow_blank: false

end
