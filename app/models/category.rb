class Category < ApplicationRecord

  belongs_to :parent, class_name: 'Category', optional: true

  has_many :subcategories, class_name: 'Category', foreign_key: :parent_id

  validates :url, presence: true, uniqueness: true, length: (5..100)
  validates :name, presence: true, length: (5..250)

  scope :catalogs, -> { where(parent: nil).includes(:subcategories) }

  # TODO find usages. add test. and rename?
  scope :only_categories, -> { where('parent_id is not null') }

end
