class BookImage < ApplicationRecord

  belongs_to :book

  mount_uploader :image, BookImageUploader

  after_destroy :delete_files

  def delete_files
    remove_image!
  end

  def file_exist?
    image.present? && File.exist?(image.path)
  end

end
