class Book < ApplicationRecord

  belongs_to :category

  has_many :images, class_name: 'BookImage'
  accepts_nested_attributes_for :images, allow_destroy: true

  scope :from_catalog, ->(catalog) { where(category_id: catalog.subcategories.pluck(:id) << catalog.id) }

  validates :url, presence: true, uniqueness: true, length: (5..100)
  validates :name, presence: true, length: (5..250)
  validates :text, presence: true, length: {minimum: 5}
  validates :price, presence: true, numericality: {greater_than: 0}

end
