class Client < ApplicationRecord

  belongs_to :delivery, class_name: 'DeliveryType'
  belongs_to :payment, class_name: 'PaymentType'

  validates :name, presence: true
  validates :phone, presence: true

  def to_s
    name
  end

end
